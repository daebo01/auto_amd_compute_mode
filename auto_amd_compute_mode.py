#python version 2

import _winreg, os

#key list
#_winreg.HKEY_CLASSES_ROOT
#_winreg.HKEY_CURRENT_USER
#_winreg.HKEY_LOCAL_MACHINE
#_winreg.HKEY_USERS
#_winreg.HKEY_PERFORMANCE_DATA
#_winreg.HKEY_CURRENT_CONFIG
#_winreg.HKEY_DYN_DATA

PROGRAMNAME = 'auto amd compute mode'
PROGRAMVERSION = '2'

HIVE = _winreg.HKEY_LOCAL_MACHINE
DISPLAYCLASS = 'System\CurrentControlSet\Control\Class\{4d36e968-e325-11ce-bfc1-08002be10318}'
TARGETVALUENAME = 'KMD_EnableInternalLargePage'
TARGETVALUEENABLEVALUE = 2
TARGETVALUEDISABLEVALUE = 0
SEARCHVALUENAME = 'DriverDesc'

def reg_key_list(key, sub_key):
	handle = _winreg.OpenKey(key, sub_key)
	retlist = []
	
	if handle:
		i = 0
		while(True):
			try:
				retlist.append(_winreg.EnumKey(handle, i))
				i = i + 1
				
			except WindowsError as e:
				break
				
	handle.Close()
	return retlist

def reg_value_list(key, sub_key):
	handle = _winreg.OpenKey(key, sub_key)
	retlist = []
	
	if handle:
		i = 0
		while(True):
			try:
				retlist.append(_winreg.EnumValue(handle, i))
				i = i + 1
				
			except WindowsError as e:
				break
				
	handle.Close()
	return retlist

def reg_value_set(key, sub_key, value_name, type, value):
	try:
		handle = _winreg.OpenKey(key, sub_key, 0, _winreg.KEY_SET_VALUE)
		_winreg.SetValueEx(handle, value_name, 0, type, value)
		handle.Close()
		
	except WindowsError as e:
		print(e)
		return False
		
	else:
		return True
		
def reg_value_get(key, sub_key, value_name):
	valuelist = reg_value_list(key, sub_key)
	
	ret = None
	for entry in valuelist:
		if entry[0] == value_name:
			ret = entry

	return ret
	
if __name__ == "__main__":
	displaylist = reg_key_list(HIVE, DISPLAYCLASS)
	
	found = False
	value = 0
	
	print('%s version %s' % (PROGRAMNAME, PROGRAMVERSION))
	while(True):
		print('0 -> compute mode disable, 1 -> compute mode enable')
		print('input : ')
		buf = raw_input()
		
		if buf == '0':
			value = TARGETVALUEDISABLEVALUE
			break
			
		elif buf == '1':
			value = TARGETVALUEENABLEVALUE
			break
			
		else:
			print('wrong input')
		
	for entry in displaylist:
		if entry == 'Properties':
			continue
		
		displayname = reg_value_get(HIVE, '\\'.join((DISPLAYCLASS, entry)), SEARCHVALUENAME)


		if displayname[1].lower().find('radeon') >= 0:
			found = True
		
			if reg_value_set(HIVE, '\\'.join((DISPLAYCLASS, entry)), TARGETVALUENAME, _winreg.REG_DWORD, value) is False:
				print('error!!')
			
			else:
				print('%s %s %s' % (displayname[1], entry, 'changed!'))
	
	if found:
		print('Done!')
	
	else:
		print('Not Found!')
	
	os.system('pause')
